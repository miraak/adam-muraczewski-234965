tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
import java.lang.*;
import java.lang.StringBuilder;
}

@members {
  Integer numer = 0;
  Integer condition_number = 0;
  Integer loop_number = 0;
  String deklaracje= new String();
}
prog    : (e+=expr | decl | e+=loop_while | e+=loop_do | e+=loop_for)* -> program(name={$e},deklaracje={deklaracje})
        ;

decl  : ^(VAR i1=ID) {globals.newSymbol($ID.text); deklaracje+=("DD "+$ID.text+"\n");}
      ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

loop_while :
         ^(LOOP_WHILE cond=expr (body+=expr | decl | body+=loop_while | body+=loop_do | body+=loop_for)*) {loop_number++;} -> while(cond={$cond.st}, body={$body}, n={loop_number.toString()})
         ;
         
loop_do :  ^(LOOP_DO cond=expr (body+=expr | decl | body+=loop_while | body+=loop_do | body+=loop_for)*) {loop_number++;} -> do_while(cond={$cond.st}, body={$body}, n={loop_number.toString()})
        ;
        
loop_for : ^(LOOP_FOR init=expr cond=expr every=expr (body+=expr | decl | body+=loop_while | body+=loop_do | body+=loop_for)*) {loop_number++;} -> for(init={$init.st}, cond={$cond.st}, every={$every.st}, body={$body}, n={loop_number.toString()})
        ;

expr    : ^(EQUAL e1=expr e2=expr) {condition_number++;} -> condition(jump={"JE"},p1={$e1.st},p2={$e2.st},n={condition_number.toString()})
        | ^(NEQUAL e1=expr e2=expr) {condition_number++;} -> condition(jump={"JNE"},p1={$e1.st},p2={$e2.st},n={condition_number.toString()})
        | ^(GREATER e1=expr e2=expr) {condition_number++;} -> condition(jump={"JG"},p1={$e1.st},p2={$e2.st},n={condition_number.toString()})
        | ^(GREATEREQ e1=expr e2=expr) {condition_number++;} -> condition(jump={"JGE"},p1={$e1.st},p2={$e2.st},n={condition_number.toString()})
        | ^(LESS e1=expr e2=expr) {condition_number++;} -> condition(jump={"JL"},p1={$e1.st},p2={$e2.st},n={condition_number.toString()})
        | ^(LESSEQ e1=expr e2=expr) {condition_number++;} -> condition(jump={"JLE"},p1={$e1.st},p2={$e2.st},n={condition_number.toString()})
        | ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.getSymbol($ID.text);} -> podstaw(id={$i1.text},p1={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | i1=ID                    {globals.getSymbol($ID.text);} -> pobierz(id={$i1.text})
        ;