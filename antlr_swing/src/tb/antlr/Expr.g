grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (instr )+ EOF!;

instr :
    (stat_nl
    | loop_while
    | loop_do
    | loop_for)
    ;

stat_nl :
    stat NL!
    | NL ->
    ;

stat
    : expr -> expr
//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID -> ^(VAR ID)
    | ID PODST expr -> ^(PODST ID expr)
    ;

loop_while
    : LOOP_WHILE^ LP! expr RP! LCB! (instr )+ RCB!
    ;

loop_do
    : LOOP_DO LCB (instr )+ RCB LOOP_WHILE LP expr RP NL-> ^(LOOP_DO expr (instr )+)
    ;

loop_for
    : LOOP_FOR LP stat_nl stat_nl stat RP LCB (instr )+ RCB -> ^(LOOP_FOR stat_nl stat_nl stat (instr )+)
    ;
    
expr
    : addExpr 
    ( EQUAL^ addExpr
    | NEQUAL^ addExpr
    | GREATER^ addExpr
    | GREATEREQ^ addExpr
    | LESS^ addExpr
    | LESSEQ^ addExpr
    )*
    ;

addExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

LOOP_WHILE: 'while';
LOOP_DO: 'do';
LOOP_FOR: 'for';

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : ';' ;

WS : ('\r' | '\n' | ' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

EQUAL
  : '=='
  ;
  
NEQUAL
  : '!='
  ;

GREATER : '>';
GREATEREQ : '>=';
LESS : '<';
LESSEQ : '<=';

LCB
  : '{'
  ;

RCB
  : '}'
  ;
  